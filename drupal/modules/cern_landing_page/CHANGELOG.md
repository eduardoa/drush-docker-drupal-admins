Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.2.0] - 21-01-2019

### Fixed
- "Non-translatable fields can only be changed when updating the current revision." error

### Removed
- Dependency with Content Translation module.


## [2.1.0] - 10-12-2018

### Changed
- File structure

