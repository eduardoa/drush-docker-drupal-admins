(function ($, Drupal) {

    'use strict';

    Drupal.behaviors.CERNComponentsFAQList = {
        attach: function (context) {
            jQuery('.panel-title').once().on('click', function(){
			    var link = jQuery(this).children('a');
				if (link.hasClass("is-open")) {
                    link.removeClass('is-open');
				} else {
                    link.addClass('is-open');
				}
			});
        }  
    };

})(jQuery, Drupal);