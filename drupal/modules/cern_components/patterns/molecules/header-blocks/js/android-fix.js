var ua = navigator.userAgent;
if (ua.indexOf('Chrome') > -1 && ua.indexOf('Mobile') > -1) {
  (function ($) {
    $(document).ready(function() {
      $('.owl-dots').first().css('bottom', '55px');
      $('.component-header__scroll').first().css('bottom', '55px');
    });
  })(jQuery);
}
