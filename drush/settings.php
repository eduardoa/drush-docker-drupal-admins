<?php

$databases['default']['default'] = array (
  'database' => "$DRUSH_DATABASE",
  'username' => "$DRUSH_USERNAME",
  'password' => "$DRUSH_PASSWORD",
  'host'     => "$DRUSH_HOST",
  'port'     => "$DRUSH_PORT",
  'prefix' => '',
  'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
  'driver' => 'mysql',
);

if (file_exists("/drupal/$DRUPAL_VERSION/sites/$DRUSH_SITENAME/private/.docker.rerun.flag")) {
  $databases['default']['default']['password'] = null;
  print("The docker image has been updated. Please exit right now and rerun it!\n");
  exit;
}

$settings['hash_salt'] = hash("sha256","$DRUSH_USERNAME" . "$DRUSH_HOST" . "$DRUSH_PASSWORD");
$settings['install_profile'] = 'cern';
$config_directories['sync'] = "sites/$DRUSH_SITENAME/files/config_/sync";

$trusted_host_pattern="^". str_replace(".","\.","$DRUSH_SITENAME") . "$";

$settings['trusted_host_patterns'] = array(
  $trusted_host_pattern,
);

$settings['file_private_path'] = "/drupal/sites/$DRUSH_SITENAME/private";

$settings['simplesamlphp_dir'] = '/var/simplesamlphp';
